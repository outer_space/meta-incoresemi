FILESEXTRAPATHS:prepend:chromite-h := "${THISDIR}/${PN}:"

BRANCH = "pmp_jugaad_v0.9_incoresemi"
SRCREV = "8642832ffbaa1c9d11f8690eada251f0be1a49f2"
SRC_URI = "git://gitlab.com/outer_space/opensbi.git;branch=${BRANCH};protocol=https; \
	file://0001-Makefile-Don-t-specify-mabi-or-march.patch \
"

SRC_URI[sha256sum] = "b011fb1f77932b04c89b3ff1fd264ab2fe97fbe500045a657a198968b00110cf"

LIC_FILES_CHKSUM = "file://COPYING.BSD;md5=42dd9555eb177f35150cf9aa240b61e5"

EXTRA_OEMAKE:append = " FW_FDT_PATH=${DEPLOY_DIR_IMAGE}/${RISCV_SBI_FDT}"
