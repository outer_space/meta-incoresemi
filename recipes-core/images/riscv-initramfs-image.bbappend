COMPATIBLE_MACHINE = "chromite-h"
EXTRA_IMAGEDEPENDS = ""
USE_DEVFS = "0"
inherit extrausers


EXTRA_USERS_PARAMS = "usermod -p `openssl passwd incore` root;"

PACKAGE_INSTALL:append = " vim-tiny"
