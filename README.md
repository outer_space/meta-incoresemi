InCoreSemi OpenEmbedded Layer
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This is a OpenEmbedded based meta layer for the InCoreSemi SoC core families like Chromite, etc.


Steps to install required software for the OpenEmbedded:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Please refer https://www.openembedded.org/wiki/Getting_started


Steps to create workspace:
~~~~~~~~~~~~~~~~~~~~~~~~~

Execute the following commands

mkdir riscv-incoresemi && cd riscv-incoresemi
repo init -u https://gitlab.com/incoresemi/software/meta-incoresemi -m tools/manifests/incoresemi.xml
repo sync


Steps to setup build environment:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Execute the following command

. ./meta-incoresemi/setup.sh

Note:
A riscv64 cross compiler will be built by the openembedded build system itself which will be then used to compile the bootloader, linux and applications


Steps to build:
~~~~~~~~~~~~~~

Execute the following command

MACHINE=chromite-h bitbake opensbi


Built Image files:
~~~~~~~~~~~~~~~~~

The following file (from build folder) is the output image file based on initramfs which can be loaded to the FPGA board using riscv64-unknown-elf-gdb (built from https://github.com/riscv/riscv-gnu-toolchain with --with-arch=rv64imac --with-abi=lp64 --with-cmodel=medany options)

tmp-glibc/deploy/images/chromite-h/fw_payload.elf

The following file (from build folder) is the output vmlinux file with debug info which can be used for gdb debugging

build/tmp-glibc/work/chromite_h-oe-linux/linux-mainline/<xyz>/linux-chromite_h-standard-build/vmlinux

Note:
Replace <xyz> with the current folder created

